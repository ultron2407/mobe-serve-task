import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridListComponent } from './grid-list/grid-list.component';
import { ListViewComponent } from './list-view/list-view.component';

const routes: Routes = [

  {
    path: "gridList",
    component: GridListComponent
  },
  {
    path: "listView",
    component: ListViewComponent
  },
  { path: '', redirectTo: '/gridList', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
